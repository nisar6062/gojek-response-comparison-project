# Gojek-Response-Comparison-Project

This project compare the http responses for the given http requests from test_data_1.txt & test_data_2.txt files in the src/main/resources folder

## Installation

### Requirements
* maven

`$ mvn clean install`

## How to Run
Run the `com.gojek.response_comparison.ResponseComparison` class 