package com.gojek.response_comparison;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Hello world!
 *
 */
public class ResponseComparison {
	public static void main(String[] args) {
		ResponseComparison rc = new ResponseComparison();
		 rc.readFile();
		// https://reqres.in/api/users/3
		// https://jsonplaceholder.typicode.com/posts
	}

	private static final String FILENAME_1 = "src/main/resources/test_data_1.txt";
	private static final String FILENAME_2 = "src/main/resources/test_data_2.txt";

	private void readFile() {
		BufferedReader br1 = null;
		BufferedReader br2 = null;
		FileReader fr1 = null;
		FileReader fr2 = null;
		try {
			fr1 = new FileReader(FILENAME_1);
			fr2 = new FileReader(FILENAME_2);
			br1 = new BufferedReader(fr1);
			br2 = new BufferedReader(fr2);
			String sCurrentLine1, sCurrentLine2;
			while ((sCurrentLine1 = br1.readLine()) != null && (sCurrentLine2 = br2.readLine()) != null) {
				String response1 = processHttpRequest(sCurrentLine1);
				String response2 = processHttpRequest(sCurrentLine2);
				System.out.println(sCurrentLine1 + (response2.equals(response1)?" equals ":" not equals ") + sCurrentLine2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br1 != null)
					br1.close();
				if (fr1 != null)
					fr1.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private String processHttpRequest(String requestURL) {
		String output = null;
		try {
			System.setProperty("http.agent", "Chrome");
			URL url = new URL(requestURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("access-control-allow-origin", "*");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String temp = null;
			while ((temp = br.readLine()) != null) {
				output=temp;
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
}
